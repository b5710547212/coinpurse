package coinpurse;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Woramate Jumroonsilp
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {

        PurseGUI purseGui = new PurseGUI();
        PurseProgGUI prog = new PurseProgGUI();
        purseGui.run();
        prog.run();
        
    	Purse purse = new Purse(10 , new RecursiveWithdraw());
    	purse.addObserver(purseGui);
    	purse.addObserver(prog);
    	ConsoleDialog ui = new ConsoleDialog( purse );
    	ui.run();

    }
}
