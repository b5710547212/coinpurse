package coinpurse;
import java.util.Comparator;
/**
 * 
 * @author Woramate Jumroonsilp
 *
 */
public class MyComparator implements Comparator<Valuable>{

    @Override
    public int compare(Valuable a, Valuable b) {
       return (int)Math.signum(a.getValue()-b.getValue());
    }
    
}
