package coinpurse;
/**
 * 
 * @author Woramate Jumroonsilp
 *
 */
public class BankNote extends AbstractValuable{

    private double value;
    private String currency;
    private long serialNumber;
    static long nextSerialNumber = 999999;
    
    /**
     * @param value Value of bank note
     *
     */
    public BankNote( double value, String currency )
    {
        this.value=value;
        this.currency = currency;
        this.serialNumber=getNextSerialNumber();
    }
    
    /**
     * 
     * @return next serial number
     */
    static long getNextSerialNumber()
    {
        return ++nextSerialNumber;
    }
    
    /**
     * @return value of banknote
     */
    public double getValue() {
        return this.value;
    }

    /**
     * @return String
     */
    public String toString()
    {
        return String.format("%3.0f "+this.currency+" Banknote [%s]", this.value,this.serialNumber);
    }
}
