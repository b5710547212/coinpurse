package coinpurse;
/**
 * 
 * @author Woramate Jumroonsilp
 *
 */
public interface Valuable extends Comparable<Valuable>{

    /**
     * 
     * @return value
     */
    public double getValue();
    /**
     * 
     * @return string
     */
    public String toString();
    /**
     * 
     * @param obj 
     * @return true if same
     */
    public boolean equals(Object obj);
    
}
