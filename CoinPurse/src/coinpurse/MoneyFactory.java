package coinpurse;

import java.util.ResourceBundle;

/**
 * 
 * @author Woramate Jumroonsilp
 *
 */
public abstract class MoneyFactory {

    private static MoneyFactory instance;
    
    protected MoneyFactory() {}
    
    /**
     * set the currency of money.
     */
    static void setMoneyFactory() {
        
        ResourceBundle bundle = ResourceBundle.getBundle("MoneyFactory");
        String type = bundle.getString("type");
        try {
            instance = (MoneyFactory)Class.forName(type).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            instance = new ThaiMoneyFactory();
        }        
    }
    
    /**
     * get instance
     * @return instance
     */
    static MoneyFactory getInstance() {
        setMoneyFactory();
        return instance;
    }
    
    /**
     * create money with double value.
     * @param value
     * @return
     */
    abstract Valuable createMoney( double value );
    
    /**
     * create money with string value
     * @param value
     * @return
     */
    Valuable createMoney( String value ) {
        return createMoney( Double.parseDouble( value ) );
    }
    
}
