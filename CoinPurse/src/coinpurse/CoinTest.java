package coinpurse;
/**
 * 
 * @author Woramate Jumroonsilp
 *
 */
public class CoinTest {
/**
 * 
 * @param args args
 */
	public static void main( String[] args )
	{
		Coin one = new Coin(1);
		Coin two = new Coin(2);
		Coin five = new Coin(5);
		Coin ten = new Coin(10);
		System.out.println(one.getValue());
		System.out.println(one.equals(two));
		System.out.println(five.compareTo(ten));
		System.out.println(ten.compareTo(five));
	}
	
}
