package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * This Gui will show how much money in your purse.
 * @author Woramate Jumroonsilp
 *
 */
public class PurseGUI extends JFrame implements Observer{

    protected static JLabel balance;
    private JPanel mainPanel;
    
    /**
     * Constructor for Gui.
     */
    public PurseGUI()
    {
        super("Purse Balance");
    }
    
    /**
     * initial components for using in gui
     */
    public void initComponent()
    {
        super.setSize(100, 100);
        
        balance = new JLabel("0.0 Baht");
        
        this.mainPanel = new JPanel();
        this.mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
        this.mainPanel.add(balance);
        
        super.add(mainPanel);
    }
    
    /**
     * update receives notification from the purse
     */
    public void update(Observable subject, Object info) {
        if(subject instanceof Purse){
            Purse purse = (Purse)subject;
            double balance = purse.getBalance();
            this.balance.setText(purse.getBalance()+"");
            System.out.println("Balance is: "+balance);
        }
        if(info!=null)
            System.out.println(info);
    }
    
    /**
     * Run Gui.
     */
    public void run()
    {
        this.initComponent();
        this.setVisible(true);
    }
    
}
