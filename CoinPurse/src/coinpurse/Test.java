package coinpurse;
/**
 * 
 * @author Woramate Jumroonsilp
 *
 */
public class Test {

    /**
     * 
     * @param args ih
     */
    public static void main(String[] args)
    {
        Purse purse = new Purse(5);
        
        Coin coin1 = new Coin(1);
        Coin coin2 = new Coin(2);
        Coin coin5 = new Coin(5);
        Coin coin10 = new Coin(10);
        
        Coupon couponBlue = new Coupon("Blue");
        
        purse.insert(coin10);
        purse.insert(couponBlue);
        
        System.out.println(purse.getBalance());
        System.out.println(purse.count());
        
        Valuable[] stuff = purse.withdraw(60);
        
        System.out.println(stuff[0].toString());
        
        System.out.println(stuff[1].toString());
        
        System.out.println(purse.getBalance());
        
    }
    
}
