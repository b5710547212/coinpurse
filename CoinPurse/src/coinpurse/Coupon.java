package coinpurse;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Woramate Jumroonsilp
 *
 */
public class Coupon extends AbstractValuable{

    private String color;
    private Map<String,Double> colorValue = new HashMap<String,Double>();

    /**
     * @param color 
     */
    public Coupon(String color)
    {
        colorValue.put("red", (double) 100);
        colorValue.put("blue", (double) 50);
        colorValue.put("green", (double) 20);
        this.color=color.toLowerCase();
        if(!colorValue.containsKey(this.color))
        {
            this.color=color;
        }
    }


    public double getValue() {
        return colorValue.get(this.color);
    }

    /**
     * @return String
     */
    public String toString()
    {
        return String.format("%s coupon", color);
    }
}
