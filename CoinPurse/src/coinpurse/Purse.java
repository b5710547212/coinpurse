package coinpurse;

import java.util.Arrays;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Observable;

import coinpurse.strategy.WithdrawStrategy;

/**
 * A coin purse contains Money. You can insert Money, withdraw money, check the balance, and check
 * if the purse is full. When you withdraw money, the coin purse decides which Money to remove.
 * 
 * @author Woramate Jumroonsilp
 */
public class Purse extends Observable {

    private List<Valuable> money;
    private int capacity;
    private WithdrawStrategy strategy;

    /**
     * Create a purse with a specified capacity.
     * 
     * @param capacity
     *            is maximum number of Money you can put in purse.
     */
    
    public Purse(int capacity) {
        this.capacity = capacity;
    }
    public Purse(int capacity, WithdrawStrategy strategy) {
        this.capacity = capacity;
        this.money = new ArrayList<Valuable>();
        setWithdrawStrategy(strategy);
    }

    /**
     * Count and return the number of Money in the purse. This is the number of Money, not their
     * value.
     * 
     * @return the number of Money in the purse
     */
    public int count() {
        return this.money.size();
    }

    public void setWithdrawStrategy(WithdrawStrategy strategy) {
        this.strategy = strategy;
    }

    /**
     * Get the total value of all items in the purse.
     * 
     * @return the total value of items in the purse.
     */
    public double getBalance() {
        double product = 0;
        for (int i = 0; i < this.money.size(); i++) {
            product += this.money.get(i).getValue();
        }
        return product;
    }

    /**
     * Return the capacity of the coin purse.
     * 
     * @return the capacity
     */
    public int getCapacity() {
        return this.capacity;
    }

    /**
     * Test whether the purse is full. The purse is full if number of items in purse equals or
     * greater than the purse capacity.
     * 
     * @return true if purse is full.
     */
    public boolean isFull() {
        return this.money.size() >= this.capacity;
    }

    /**
     * Insert a coin into the purse. The coin is only inserted if the purse has space for it and the
     * coin has positive value. No worthless Money!
     * 
     * @param value
     *            is a valuable object to insert into purse
     * @return true if coin inserted, false if can't insert
     */
    public boolean insert(Valuable value) {
        // if the purse is already full then can't insert anything.
        if (!isFull()) {
            this.money.add(value);
            super.setChanged();
            super.notifyObservers(this);
            return true;
        }
        super.setChanged();
        super.notifyObservers(this);
        return false;
    }

    /**
     * Withdraw the requested amount of money. Return an array of Money withdrawn from purse, or
     * return null if cannot withdraw the amount requested.
     * 
     * @param amount
     *            is the amount to withdraw
     * @return array of Coin objects for money withdrawn, or null if cannot withdraw requested
     */
    public Valuable[] withdraw(double amount) {
        if (amount == 0)
            return null;
        Valuable[] cache = this.strategy.withdraw(amount, this.money);
        if (cache != null)
            for (Valuable val : cache)
                money.remove(val);

        super.setChanged();
        super.notifyObservers(this);
        return cache;
    }

    /**
     * toString returns a string description of the purse contents. It can return whatever is a
     * useful description.
     * 
     * @return String of object
     */
    public String toString() {
        double sum = 0;
        for (int i = 0; i < this.money.size(); i++) {
            sum += money.get(i).getValue();
        }
        String product = "";
        product += this.money.size() + " Money with value " + sum;
        return product;
    }

}
