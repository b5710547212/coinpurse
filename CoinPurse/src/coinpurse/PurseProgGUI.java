package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * This Gui will show how much money in your purse.
 * @author Woramate Jumroonsilp
 *
 */
public class PurseProgGUI extends JFrame implements Observer{

    private int size, contain;
    private JProgressBar progress;
    private JPanel mainPanel;
    private JLabel head;

    /**
     * Constructor for gui
     */
    public PurseProgGUI ()
    {
        super("Purse Status");
    }

    /**
     * initial components for gui
     */
    public void initComponents()
    {
        super.setSize(100, 100);

        progress = new JProgressBar(contain,size);

        head = new JLabel("Empty");

        mainPanel = new JPanel();
        this.mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
        this.mainPanel.add(progress);
        mainPanel.add(head);

        super.add(mainPanel);
    }

    /**
     * update receives notification from the purse
     */
    public void update(Observable subject, Object info) {
        if(subject instanceof Purse){
            Purse purse = (Purse)subject;
            this.size = purse.getCapacity();
            this.contain = purse.count();
            this.progress.setMaximum(size);
            this.progress.setValue(contain);
            if(this.contain==0)
            {
                this.head.setText("Empty");
            }
            else if(this.contain<this.size)
            {
                this.head.setText(this.contain+"");
            }
            else
            {
                this.head.setText("Full");
            }
        }
        if(info!=null)
            System.out.println(info);
    }

    /**
     * run gui
     */
    public void run()
    {
        this.initComponents();
        super.setVisible(true);
    }

}
