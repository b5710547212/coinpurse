package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.MyComparator;
import coinpurse.Valuable;

public class GreedyWithdraw implements WithdrawStrategy{

    @Override
    public Valuable[] withdraw(double amount, List<Valuable> valuable) 
    {

        MyComparator comparator = new MyComparator();
        Collections.sort(valuable,comparator);
        Collections.reverse(valuable);
        ArrayList<Valuable> temp = new ArrayList<Valuable>();
        
        for(int i = 0;i<valuable.size();i++)
        {
            if(amount==0)
                break;
            if(amount<0)
                break;
            if(valuable.get(i).getValue()>amount)
                continue;
            temp.add(valuable.get(i));
            amount-=valuable.get(i).getValue();
        }
        if(amount!=0)
            return null;

        Valuable[] product = new Valuable[temp.size()];
        for(int i = 0;i<temp.size();i++)
        {
            product[i]=temp.get(i);
        }

        for(int i = 0;i<temp.size();i++)
        {
            for(int j = 0;j<valuable.size();j++)
            {
                if(temp.get(i).equals(valuable.get(j)))
                {
                    valuable.remove(j);
                    break;
                }
            }
        }
        return product;
    }
}
