package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.MyComparator;
import coinpurse.Valuable;

public class RecursiveWithdraw implements WithdrawStrategy {

    @Override
    public Valuable[] withdraw(double amount, List<Valuable> valuable) {
        Collections.sort(valuable, new MyComparator());
        Collections.reverse(valuable);
        List<Valuable> fromTo = withdrawTo(amount, valuable, 0);
        if (fromTo == null && valuable.size() == 1)
            return null;

        if (fromTo == null) {
            return withdraw(amount, valuable.subList(1, valuable.size()));
        } else {
            Valuable[] product = new Valuable[fromTo.size()];
            for (int j = 0; j < fromTo.size(); j++) {
                product[j] = fromTo.get(j);
            }
            return product;
        }
    }

    private static List<Valuable> withdrawTo(double amount, List<Valuable> valuable, int i) {
        if (valuable.size() == 0) {
            return null;
        }
        if (i == valuable.size()) {
            return null;
        }
        amount -= valuable.get(i).getValue();
        if (amount == 0) {
            List<Valuable> pro = new ArrayList<Valuable>();
            pro.add(valuable.get(i));
            return pro;
        }
       
        if (amount < 0) {
            return null;
        }
      
        List<Valuable> temp = withdrawTo(amount, valuable, i + 1);
        if (temp == null) {
            amount += valuable.get(i).getValue();
            return withdrawTo(amount, valuable, i+1);
        } else {
            temp.add(valuable.get(i));
            return temp;
        }
    }
}
