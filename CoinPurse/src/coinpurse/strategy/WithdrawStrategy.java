package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;
/**
 * 
 * @author Woramate Jumroonsilp
 *
 */
public interface WithdrawStrategy {
    /**
     * 
     * @param amount
     * @param valuable
     */
    public Valuable[] withdraw(double amount , List<Valuable> valuable);

}
