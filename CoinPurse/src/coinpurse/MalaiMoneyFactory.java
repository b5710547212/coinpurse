package coinpurse;

import java.util.Arrays;
import java.util.List;

public class MalaiMoneyFactory extends MoneyFactory{

    private final String currencyName = "Ringgit";
    private final String subCurrencyName = "Sen";
    
    @Override
    Valuable createMoney(double value) {
        List<Double> coinVal = Arrays.asList(0.05,0.10,0.20,0.50);
        List<Double> bankVal = Arrays.asList(1.0,2.0,5.0,10.0,20.0,50.0,100.0);
        if( coinVal.contains( value ) )
            return new Coin( value, this.subCurrencyName );
        else if( bankVal.contains( value ) )
            return new BankNote( value, this.currencyName );
        else throw new IllegalArgumentException();
    }
}
