package coinpurse;
/**
 * A super class that contain subclassm of banknote coin and cou[on
 * @author Woramate Jumroonsilp
 *
 */
public abstract class AbstractValuable implements Valuable{

    protected double value;  
    
    public AbstractValuable() 
    {
        super();
    }

    /**
     * @param obj object to be compare
     * @return true if same banknote
     */
    public boolean equals(Object obj) {
        if(obj!=null){
            if(this.getClass()==obj.getClass()){
                Valuable other = (Valuable)obj;
                if(this.getValue()==other.getValue()){
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * use to compare two value
     * @param other
     * @return
     */
    public int compareTo(Valuable other)
    {
        if(this.getValue()>other.getValue())
        {
            return 1;
        }
        else if(this.getValue()<other.getValue())
        {
            return -1;
        }
        else
            return 0;
    }

}