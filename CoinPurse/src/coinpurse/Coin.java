package coinpurse;


/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Woramate Jumroonsilp
 */
public class Coin extends AbstractValuable{

	private double value;
	private String currency;
	/** 
	 * Constructor for a new coin. 
	 * @param value is the value for the coin
	 */
	public Coin( double value, String currency ) {
		this.value = value;
	    this.currency = currency;
	}
	/**
	 * getValue return value of this coin.
	 * @return value of coin
	 */
	public double getValue()
	{
		return this.value;
	}

	/**
	 * @return String
	 */
	public String toString()
	{
		return String.format("%.0f-"+this.currency+" coin", value);
	}
}
